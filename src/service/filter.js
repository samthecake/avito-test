import { inFavorite } from "./favorites";

export const filters = {
  category: (value, products) => products.filter(product => product.category === value),
  priceFrom: (value, products) => products.filter(product => product.price >= value),
  priceTo: (value, products) => products.filter(product => product.price <= value),
  inFavorites: (value, products) => (value ? products.filter(product => inFavorite(product.id)) : products)
};

export function filter(products, activeFilters) {
  if (activeFilters.length === 0) {
    return products;
  }

  let filteredProducts = [...products];
  for (const activeFilter in activeFilters) {
    filteredProducts = filters[activeFilter]
      ? filters[activeFilter](activeFilters[activeFilter], filteredProducts)
      : filteredProducts;
  }

  return filteredProducts;
}
