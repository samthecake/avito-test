const API_URL = 'https://avito.dump.academy/sellers';

export function getSellers() {
  return fetch(API_URL, { mode: 'cors' }).then(res => res.json());
}
