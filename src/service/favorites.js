import { hasKey } from './localStorage';

const FAVORITE_STORAGE_KEY = 'favorites';

export function inFavorite(productId) {
  if (!hasKey(FAVORITE_STORAGE_KEY)) {
    return false;
  }

  return JSON.parse(localStorage.getItem(FAVORITE_STORAGE_KEY)).find(fav => fav.id === productId);
}

export function addFavorite(product) {
  if (!hasKey(FAVORITE_STORAGE_KEY)) {
    localStorage.setItem(FAVORITE_STORAGE_KEY, JSON.stringify([]));
  }

  if (hasKey(FAVORITE_STORAGE_KEY) && !inFavorite(product.id)) {
    const favorites = JSON.parse(localStorage.getItem(FAVORITE_STORAGE_KEY));
    const updatedFavorites = favorites.concat([product]);

    localStorage.setItem(FAVORITE_STORAGE_KEY, JSON.stringify(updatedFavorites));
  }
}
