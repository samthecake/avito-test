const API_URL = 'https://avito.dump.academy/products';

export function getProducts() {
  return fetch(API_URL, { mode: 'cors' }).then(res => res.json());
}
