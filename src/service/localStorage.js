export function hasKey(key) {
  return localStorage.getItem(key) !== null;
}
