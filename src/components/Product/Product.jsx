import React, { Component } from 'react';
import { addFavorite, inFavorite } from '../../service/favorites';

export default class Product extends Component {
  state = {
    mainPictureKey: 0,
    inFavorite: false
  };

  handleFavoriteClick = () => {
    if (!inFavorite(this.props.product.id)) {
      addFavorite(this.props.product);
      this.setState({
        inFavorite: true
      });
    }
  };

  componentDidMount() {
    if (inFavorite(this.props.product.id)) {
      this.setState({
        inFavorite: true
      });
    }
  }

  render() {
    const { title, price, pictures } = this.props.product;

    return (
      <div className="product">
        <div className="product-title">{title}</div>
        <div className="product-images">
          <div className="product-main-image">
            <img src={pictures[this.state.mainPictureKey]} />
          </div>
          <div className="product-sub-images">
            {pictures.slice(this.state.mainPictureKey + 1, pictures.length).map((picture, i) => (
              <img key={i} src={picture} />
            ))}
          </div>
        </div>
        <div className="product-price">{price}</div>
        <div className="product-seller">
          <div className="product-seller-title" />
          <div className="product-seller-rating" />
        </div>
        <button type="button" onClick={this.handleFavoriteClick}>
          {this.state.inFavorite ? 'Удалить из избранного' : 'Добавить в избранное'}
        </button>
      </div>
    );
  }
}
