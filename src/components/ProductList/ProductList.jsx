import React, { Component } from 'react';

import Filters from '../Filters';
import Product from '../Product';
import { getProducts } from '../../service/products';
import { getSellers } from '../../service/sellers';
import { filter } from '../../service/filter';

export default class ProductList extends Component {
  state = {
    filters: [],
    sellers: [],
    products: [],
    sort: null
  };

  componentDidMount() {
    getSellers().then(sellers =>
      getProducts().then(products => this.setState({ products: products.data, sellers: sellers.data }))
    );
  }

  handleFiltersChange = filters => {
    this.setState({
      filters
    });
  };

  render() {
    return (
      <div>
        <Filters
          sellers={this.state.sellers}
          onFiltersChange={this.handleFiltersChange}
          activeFilters={this.state.filters}
        />

        {filter(this.state.products, this.state.filters).map(product => (
          <Product key={product.id} product={product} sellers={this.state.sellers} />
        ))}
      </div>
    );
  }
}
