import React, { Component } from 'react';

const categories = [
  { key: 'immovable', label: 'Недвижимость' },
  { key: 'cameras', label: 'Фотоаппараты' },
  { key: 'auto', label: 'Автомобили' },
  { key: 'laptops', label: 'Ноутбуки' }
];

export class Filters extends Component {
  state = {
    isFavoriteChecked: false
  };

  handleCategoryChange = e => {
    this.props.onFiltersChange({ ...this.props.activeFilters, category: e.target.value });
  };

  handlePriceChange = (e, type) => {
    this.props.onFiltersChange({ ...this.props.activeFilters, [type]: e.target.value });
  };

  handleFavoriteChange = e => {
    this.setState(
      prevState => ({
        isFavoriteChecked: !prevState.isFavoriteChecked
      }),
      () => this.props.onFiltersChange({ ...this.props.activeFilters, inFavorites: this.state.isFavoriteChecked })
    );
  };

  render() {
    return (
      <div className="filters">
        <div className="filter-item">
          <div className="filter-label">По категории:</div>
          <select onChange={this.handleCategoryChange}>
            <option>Выберите категорию:</option>
            {categories.map(cat => (
              <option key={cat.key} value={cat.key}>
                {cat.label}
              </option>
            ))}
          </select>
        </div>
        <div className="filter-item">
          <div className="filter-label">По цене</div>
          <div className="filter-range">
            <div className="filter-range-from">
              <input type="number" onChange={e => this.handlePriceChange(e, 'priceFrom')}/>
              <input type="number" onChange={e => this.handlePriceChange(e, 'priceTo')}/>
            </div>
            <div className="filter-range-to" />
          </div>
        </div>
        <div className="filter-item">
          <div className="filter-label">Только избранные</div>
          <input type="checkbox" onChange={this.handleFavoriteChange} />
        </div>
      </div>
    );
  }
}

export default Filters;
