import React from 'react';

import ProductList from './components/ProductList';

function App() {
  return (
    <>
      {/* <Filters /> */}
      <ProductList />
    </>
  );
}

export default App;
